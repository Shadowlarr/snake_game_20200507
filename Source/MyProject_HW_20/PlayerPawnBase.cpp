// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Snake.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	Field = 3;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	if (Field < 3) //������
	{
		Field = 3;
	}
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, FString::Printf(TEXT("Set the difficulty level in PlayerPawnBP Field.")));
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);

	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnake>(SnakeActorClass, FTransform());
	SnakeActor->PlayerPawnBaseOwner = this;
	SnakeActor->LocMath();
	SnakeActor->SpawnFood();
	SnakeActor->SpawnWalls();
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value) // ������ �� ������� ������� ��� Move � Snake �� �� ����� ��������� ������ (������ ����������)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0)
		{
			SnakeActor->MoveDirection = EMovementDirection::UP;
		}
		else if (value < 0)
		{
			SnakeActor->MoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0)
		{
			SnakeActor->MoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0)
		{
			SnakeActor->MoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::PlayerPawnBaseGameOver(ASnake* SnakeServant)
{
	if (IsValid(SnakeServant))
	{
		SnakeServant->Destroy();
	}
}

