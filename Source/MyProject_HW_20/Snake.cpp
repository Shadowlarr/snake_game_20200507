// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "PlayerPawnBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Wall.h"
#include "Interactable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElemment();
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

bool ASnake::AddSnakeElemment(int ElementsNum)  // ����� ��������� ����
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		int32 SNum = SnakeElements.Num();
		
		if (IsValid(PlayerPawnBaseOwner))
		{
			if (SNum >= (PlayerPawnBaseOwner->Field * PlayerPawnBaseOwner->Field))
			{
				GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, FString::Printf(TEXT("WIN! Game Over.")));
				SnakeGameOver(); // ��� ������ ����������� ������� "������!"
				return 1;
			}
		}

		FVector NewLocation(SNum * ElementSize, 0, 0);

		if (SNum > 0)
		{
			NewLocation.Set(0, 0, -1000); // ������� �������!!!
		}

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	return 0;
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (MoveDirection)
	{
	case EMovementDirection::UP:
		if (LastMoveDirection != EMovementDirection::DOWN)
		{
			MovementVector.X += ElementSize;
			LastMoveDirection = MoveDirection;
		}
		else
			MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::DOWN:
		if (LastMoveDirection != EMovementDirection::UP)
		{
			MovementVector.X -= ElementSize;
			LastMoveDirection = MoveDirection;
		}
		else
			MovementVector.X += ElementSize;
		break;
	case EMovementDirection::LEFT:
		if (LastMoveDirection != EMovementDirection::RIGHT)
		{
			MovementVector.Y += ElementSize;
			LastMoveDirection = MoveDirection;
		} 
		else
			MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		if (LastMoveDirection != EMovementDirection::LEFT)
		{
			MovementVector.Y -= ElementSize;
			LastMoveDirection = MoveDirection;
		}
		else
			MovementVector.Y += ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();						// ��������� ������������

	for (int i = SnakeElements.Num() - 1; i > 0; --i)			// �������� ������ ������������ � �������� ���� �� �����
	{
		ASnakeElementBase* CurrentElement = SnakeElements[i];
		ASnakeElementBase* PrevElement = SnakeElements[i - 1];
		FVector PrevLocaton = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocaton);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();						// �������� ������������
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);						// ����� � ������� "��� �����" ������� � ������ ��� ������
		bool BeIsFirst = (ElemIndex == 0);										// ���� ������ =0, �� ���� ������� - ������, 
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);		// ������� ������ ����� � �����������. ������� ������� �� ������� �������.
		
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, BeIsFirst);
		}
	}
}

bool ASnake::SpawnFood() // ��������� ��� ������
{
	FVector NewLocation(ForceInitToZero);
	FRandomStream RandStream;
	RandStream.GenerateNewSeed();
	NewLocation.X = RandStream.RandRange((-1) * (LocFrom - 1 - LocSize), LocTo - (LocSquare % 2)) * ElementSize;
	RandStream.GenerateNewSeed();
	NewLocation.Y = RandStream.RandRange((-1) * (LocFrom - 1 - LocSize), LocTo - (LocSquare % 2)) * ElementSize;

	for (int i = SnakeElements.Num()-1; i >= 0; --i)
	{
		if (NewLocation == SnakeElements[i]->GetActorLocation())
		{
			SpawnFood();
			return 0;
		}
	}

	FTransform NewTransform(NewLocation);
	AFood* NewSnakeElem = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	return 0;
}

void ASnake::DestroyFood(AActor* Food)
{
	Food->Destroy();
}

void ASnake::SnakeGameOver()
{
	if (IsValid(PlayerPawnBaseOwner))
	{
		PlayerPawnBaseOwner->PlayerPawnBaseGameOver(this);
	}
}

void ASnake::SpawnWalls()			// �������
{
	CreateWall(EMovementDirection::UP, LocTo);
	CreateWall(EMovementDirection::DOWN, LocFrom);
	CreateWall(EMovementDirection::LEFT, LocFrom);
	CreateWall(EMovementDirection::RIGHT, LocTo);
}

void ASnake::CreateWall(EMovementDirection WallLocation, int32 &Size)
{
	FRotator NewRotation(0, 0, 0);
	FVector NewTranslation(0, 0, 0);
	FVector NewScale3D(1, 1, 1);

	switch (WallLocation)
	{
	case EMovementDirection::UP:
		NewTranslation.X = (Size + LocSize) * ElementSize;
		NewTranslation.Y = 0.5 * LocSize * ElementSize;
		NewScale3D.Y = PlayerPawnBaseOwner->Field + 2;
		break;
	case EMovementDirection::DOWN:
		NewTranslation.X = (-Size + LocSize) * ElementSize;
		NewTranslation.Y = 0.5 * LocSize * ElementSize;
		NewScale3D.Y = PlayerPawnBaseOwner->Field + 2;
		break;
	case EMovementDirection::LEFT:
		NewTranslation.Y = (-Size + LocSize) * ElementSize;
		NewTranslation.X = 0.5 * LocSize * ElementSize;
		NewScale3D.X = PlayerPawnBaseOwner->Field + 2;
		break;
	case EMovementDirection::RIGHT:
		NewTranslation.Y = (Size + LocSize) * ElementSize;
		NewTranslation.X = 0.5 * LocSize * ElementSize;
		NewScale3D.X = PlayerPawnBaseOwner->Field + 2;
		break;
	default:
		break;
	}

	FTransform NewTransform(NewRotation, NewTranslation, NewScale3D);
	AWall* NewWall = GetWorld()->SpawnActor<AWall>(WallClass, NewTransform);
}

void ASnake::LocMath()
{
	if (IsValid(PlayerPawnBaseOwner))
	{
		LocSquare = PlayerPawnBaseOwner->Field;
	}

	LocFrom = (LocSquare - (LocSquare % 2)) / 2 + 1;
	LocTo = (LocFrom + (LocSquare % 2)) - 1;
	LocSize = 1 * ((LocSquare + 1) % 2);
}
