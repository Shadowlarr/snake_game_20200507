// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProject_HW_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_HW_20_API AMyProject_HW_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
