// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElementBase;
class AFood;
class APlayerPawnBase;
class AWall;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};



UCLASS()
class MYPROJECT_HW_20_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWall> WallClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	TArray<ASnakeElementBase*> SnakeElements;

	EMovementDirection LastMoveDirection;

	EMovementDirection MoveDirection;

	APlayerPawnBase* PlayerPawnBaseOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		bool AddSnakeElemment(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION(BlueprintCallable)
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	void DestroyFood(AActor* Food);

	bool SpawnFood();

	void SnakeGameOver();

	void SpawnWalls();

	void CreateWall(EMovementDirection WallLocation, int32 & Size);

	/*Math*/
	int32 LocSquare = 3;
	int32 LocFrom = 1;
	int32 LocTo = 1;
	int32 LocSize = 1;
	void LocMath();
};
