// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Snake.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* Interactor, bool BeIsHead)
{
	if (BeIsHead)
	{
		ASnake* Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, FString::Printf(TEXT("Lose. Game Over.")));
			Snake->SnakeGameOver();
		}
	}
}
